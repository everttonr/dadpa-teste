var gulp =require('gulp'),
sass = require('gulp-sass'), //funcionamento dos arquivos .sass
uglify = require('gulp-uglify'), //mimificar arquivos
concat = require('gulp-concat'), //unir arquivos js em um unico arquivo
watch = require('gulp-watch');

//task Concat * união de js
gulp.task('concat', function(){
	return gulp.src('src/js/*.js')
	.pipe(concat('all.js'))
	.pipe(uglify())
	.pipe(gulp.dest('assets/js'))
})

//task SASS
gulp.task('sass', function(){
	return gulp.src('src/sass/*.sass')
	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	.pipe(gulp.dest('assets/css'))
});
//task WATCH
gulp.task('watch', function(){
	gulp.watch('src/sass/**/*.sass', ['sass']);
});
//task default
gulp.task('default', ['sass', 'concat', 'watch']);