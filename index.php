<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Front-End</title>
	<?php include('./templates/template_head.php'); ?>
</head>
<body>
	<?php include('./templates/template_header.php'); ?>
	<!-- / header -->
	<main>
		<div class="main">
			<div class="wrap_banner">
				<div class="caption_banner container">
					<h2>Lorem Ipsum is simply dummy text</h2>
					<p class="wrap_bt">
						<a class="btn-padrao btn-azul">
							Contate-nos
						</a>
					</p>
				</div>
				<img src="./assets/images/banner.png" alt="Banner">
			</div>
			<div class="screen aclinica container">
				<div class="screen_head">
					<h2>
						A Clínica
						<span class="line"></span>
					</h2>
				</div>
				<div>
					<div class="col-xs-6">
						<h3>Sobre nós</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vulputate lacus orci. Duis ac enim quis lectus egestas vehicula in in risus. Vestibulum sollicitudin bibendum dignissim.<br>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vulputate lacus orci. Duis ac enim quis lectus egestas vehicula in in risus. Vestibulum sollicitudin bibendum dignissim. Fusce rhoncus efficitur pellentesque. </p>
						<p class="wrap_bt">
							<a class="btn-padrao btn-laranja">
								Saiba mais
							</a>
						</p>
					</div>
					<div class="col-xs-6 margin_neg">
						<figure>
							<img src="./assets/images/imagem1.png" alt="A Clínica">
						</figure>
					</div>
				</div>
			</div>
			<!-- / a clinica -->
		</div>
		<div class="screen metodos">
			<div class="main">
				<div class="screen_head">
					<h2>
						Métodos
						<span class="line"></span>
					</h2>
				</div>
				<div>
					<div class="col-xs-12"><?php include('./templates/template_metodos.php'); ?></div>
				</div>
				<p class="wrap_bt center">
					<a class="btn-padrao btn-laranja">
						Mais informações
					</a>
				</p>
			</div>
		</div>
		<!-- / métodos -->
		<div class="screen contato">
			<div class="main container">
				<div class="screen_head">
					<h2>
						Contato
						<span class="line"></span>
					</h2>
				</div>
				<div>
					<p><a class="fone" href="tel:+558134343434"><i class="fa fa-phone" aria-hidden="true"></i> (81) 3434-3434</a></p>
					<p class="wrap_bt">
						<a class="btn-padrao btn-azul">
							Saiba como chegar
						</a>
					</p>
				</div>
			</div>
		</div>
		<!-- / contato -->
		<?php include('./templates/template_footer.php'); ?>
		<!-- / footer -->
	</main>

	
</body>
</html>