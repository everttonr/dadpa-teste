<footer>
	<div class="footer">
		<div class="main container">
			<div class="row">
				<div class="rodape">
					<p>
						<span class="wrap_up">
							<i class="fa fa-angle-up" aria-hidden="true"></i>
							<span class="up"></span>
						</span>
					</p>
					<div class="social">
						<a href="#" class="face"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#" class="insta"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a href="#" class="link"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</div>
					<p>
						da-dpa <br>Copyright © 2018
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- SCRIPT -->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="./assets/js/all.js"></script>
<script>
$(function(){
  //Efeito menu sanduiche
  $('.ac-gn-menuicon-label').click(function() {
    $(this).toggleClass('touch');
    $('body').toggleClass('mobile');
  });
  $('.menu a').click(function() {
    $('body').toggleClass('mobile');
    $('*').toggleClass('touch')
  });
  //bt scroll up
  $('.wrap_up').click(function(){
    $('html, body').animate({scrollTop:0}, 600);
    return false;
  });
  //carousel
  var owl = $('.owl-carousel');
  owl.owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    navigation : true,
    navText: [ '', '' ],
    navElement: [ 'div class="fa"' ],
    autoplay: true,
    autoplayTimeout: 10000,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 4,
      }
    }
  });
})
</script>
