<!-- Favicons -->
<link rel="icon" href="./assets/img/favicon.png">
<link rel="icon" href="./assets/img/favicon-16.png" sizes="16x16">
<link rel="icon" href="./assets/img/favicon-32.png" sizes="32x32">
<link rel="icon" href="./assets/img/favicon-48.png" sizes="48x48">
<link rel="icon" href="./assets/img/favicon-64.png" sizes="64x64">
<link rel="icon" href="./assets/img/favicon-128.png" sizes="128x128">
<!-- Stylesheets -->
<link rel="stylesheet" href="./assets/css/plugin.css">
<link rel="stylesheet" href="./assets/css/build.css">