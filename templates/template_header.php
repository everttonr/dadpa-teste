<header>
	<div class="wrap_header">
		<div class="header container">
			<div class="col col-1 marca">
				<h1>
					<a href="" title="Home"><img src="./assets/img/marca.png" alt="Nome da Instituição"></a>
				</h1>
			</div>
			<nav id="ac-globalnav" class="js no-touch">
				<label class="ac-gn-menuicon-label" for="ac-gn-menustate" aria-hidden="true">
					<span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-top">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-top"></span>
					</span>
					<span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-bottom">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-bottom"></span>
					</span>
				</label>
			</nav>
			<div class="col-menu">
				<div class="col col-2">
					<nav>
						<ul>
							<li><a href="#">A Clínica</a></li>
							<li><a href="#" class="ativo">Especialidades</a></li>
							<li><a href="#">Equipe</a></li>
							<li><a href="#">Métodos</a></li>
							<li><a href="#">Convênios</a></li>
							<li><a href="#">Contato</a></li>
						</ul>
					</nav>
				</div>
				<div class="col col-3">
					<div class="social">
						<a href="#" class="face"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#" class="insta"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a href="#" class="link"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>